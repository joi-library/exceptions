/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.exceptions;

import com.github.elwinbran.javaoverhaul.strings.FormattedString;

/**
 * Tries to provide all information on an exception.
 * This class is usually referred to as if it was the exception, which is not 
 * true but this is indeed the preferred way of calling it. 
 * 
 * @author Elwin Slokker
 */
public interface ExceptionInformation
{
    /**
     * Call to get the identifier, which differentiates how the exception can
     * be solved.
     * This method should be used by exception handlers.
     * 
     * @return An object that represents the type of this exception.
     */
    public abstract ExceptionType checkType();
    
    /**
     * @return A string that explains what went wrong.
     * <br>(the unexpected situation).
     */
    public abstract FormattedString getDescription();
    
    /**
     * More information about this exception.
     * 
     * @return an object that provides more information about the exception.
     */
    public abstract ExceptionDetails details();
}
