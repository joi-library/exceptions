/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.exceptions;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.strings.FormattedString;

/**
 * Represents a type of exceptions.
 * Compare to how Java has lots of exception classes. {@code ExceptionTypes}
 * can be hierarchical.
 * 
 * Implements correct equality since no to exception types may be equal.
 * 
 * @author Elwin Slokker
 */
public interface ExceptionType
{
    /**
     * @return The name of this type.
     */
    public abstract FormattedString getName();

    /**
     * The possible parent of this type.
     * 
     * @return the parent type. Used for handlers when they only handle the 
     * 'broader' case AKA parent exception.
     */
    public abstract NullableWrapper<ExceptionType> getParent();

    /**
     * A good indication of how to solve this exception.
     * @return An object that represents the solving method.
     */
    public abstract ActionGroup getGroup();
}
