/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.exceptions;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.strings.FormattedString;

/**
 * Represents the detail information of an exception.
 * All the information in this class should be read and interpreted by humans.
 * 
 * @author Elwin Slokker
 */
public interface ExceptionDetails
{
    /**
     * @return A fully human readable message that includes all information of
     * this exception.
     */
    public abstract FormattedString fullMessage();
    
    /**
     * Use this method to wrap legacy checked exception and there is no way
     * to extract the information from the checked exception.
     * 
     * @return a wrapped throwable if there is one.
     * @deprecated The goal of this project is to deprecate checked exceptions
     * so using this method already goes against that. 
     */
    @Deprecated
    public abstract NullableWrapper<Throwable> getOriginalThrowable();
    
    /**
     * @return a string that indicates where in the source code the exception 
     * was thrown.
     */
    public abstract FormattedString exceptionSource();
}
