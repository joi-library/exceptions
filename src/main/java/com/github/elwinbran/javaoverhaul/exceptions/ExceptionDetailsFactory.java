/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.exceptions;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.strings.FormattedString;

/**
 * Produces objects that provide information about exceptions for humans.
 * 
 * @author Elwin Slokker
 * @see ExceptionDetails
 */
public interface ExceptionDetailsFactory
{
    /**
     * Produces a detail object.
     * 
     * @param fullMessage The message to appear in the detail object.
     * @param exceptionSource The source location to appear in the detail object.
     * @return A detail object without throwable and including the given details.
     */
    public abstract ExceptionDetails make(FormattedString fullMessage, 
                                          FormattedString exceptionSource);
    /**
     * @return A detail without any information at all.
     */
    public abstract ExceptionDetails makeEmpty();
    
    /**
     * 
     * @param fullMessage
     * @param exceptionSource
     * @param original
     * @return
     * @deprecated
     */
    @Deprecated
    public abstract ExceptionDetails make(FormattedString fullMessage, 
            FormattedString exceptionSource, NullableWrapper<Throwable> original);
}
