/**
 * Root package of the java exception replacement library.
 * Provides replacement for Java Exceptions.
 * 
 * //
 * STORY TIME
 * So how do exceptions work in terms of dependencies?
 * Interface X manages resources by byte read write access.
 * Class Z implements this and has access to a single local file.
 * Class Z knows:
 * -how to access file provided the arguments.
 * -how to overwrite file.
 * -what can go wrong during either behaviour.
 *    -and how to tell if something went wrong?
 * 
 * What can go wrong? 
 * -file does not exist
 * -file is not writable
 * -file is not readable.
 * -file read/write was aborted/interupted.
 * 
 * Object/process Y does the actual file accesses and exposes a primitive 
 * interface that includes these error messages.
 * 
 * At any rate, Z knows of a few exceptions because the programmer choose those.
 * What type(s) of exception(s) does X have to expose?
 * //
 * 
 * NOTE: {@link com.github.elwinbran.javaoverhaul.exceptions.Exceptional}
 * and {@link com.github.elwinbran.javaoverhaul.exceptions.SafeReturn}
 * resemble each other extremely. It cannot be recalled often enough that 
 * {@code SafeReturn} is preferable and {@code Exceptional} may be removed in 
 * a major version change.
 * 
 * Currently the only package in the Universal Exception Information library.
 * <br><br>
 * This library essentially contains two things:
 * <br>-Classes that represent the 'Exceptions' and supporting classes.
 * <br>-Constants and interfaces for the 'types' of exceptions.
 * <br>
 * This library provides a few classes and constants to 'universally' (this 
 * library strives to be very usable for every Java program) 
 * identify exceptions. 'Exceptions' are special failure cases in execution 
 * paths, like the program is unauthorised to read a file when the program needs
 * to read a configuration file.
 * 'Exceptions' are not the same as 'Errors' in this library.
 * 'Errors' being defined as problems that can occur outside of the 
 * programs/application/library's scope. This includes, but is not limited to,
 * Out of memory situations, hardware failures or unsupported operations on host
 * Operating System.
 * <br><br>
 * The aim of this library is making an exception object in the method that has
 * an exception path that is always useful to the caller (that is not limited to
 * other methods). A careful study of standard java exceptions shows that they
 * are either useless or a some casting is required. This library behaves very 
 * different from the Java Exceptions and should be more useful to the 
 * programmer, as well as make him more aware of exceptional paths.
 * 
 * <h1>ExceptionInformation</h1>
 * The name of the class that tells a caller about an exception in a called 
 * method. Mind they are called information, not exception.
 * They provide the object that encountered the exceptions, a wrapped 
 * Throwable (if there is one), an indication of the severity of the exception,
 * a string message for programmers or loggers and lastly, but most important,
 * the 'type' of the exception.
 * This type can be used by calling code to identify and handle the exception.
 * ExceptionInformation instances are made through 
 * {@link ExceptionInformationBuilder} exclusively.
 * 
 * <h1>ExceptionType</h1>
 * An exception type has a name that must be unique. No two exception types can
 * have the same name. This unique name makes them identifiable.
 * The second thing they provide is their parent type.
 * Exception handlers can ask for a parent if they do not have a handler for that type.
 * They may have one for the parent type instead.
 */
package com.github.elwinbran.javaoverhaul.exceptions;