/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.exceptions;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.strings.FormattedString;

/**
 * Produces {@link ExceptionType} objects.
 * 
 * TODO extra method for parentless?
 * 
 * @author Elwin Slokker
 * @see ExceptionType
 */
public interface ExceptionTypeFactory
{
    /**
     * Produces an exception type by providing all requirements directly.
     * 
     * @param typeName The name of the exception type.
     * @param resolve Indication of solving method.
     * @param parent The type that the produced type is a part of, which can be 
     * none and then the wrapper should {@link NullableWrapper#isNil() indicate that}.
     * @return An exception type with the provided arguments as behaviour.
     */
    public abstract ExceptionType makeType(FormattedString typeName, 
            ActionGroup resolve, NullableWrapper<ExceptionType> parent);
    
}
