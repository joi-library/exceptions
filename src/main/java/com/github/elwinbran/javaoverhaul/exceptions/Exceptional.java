/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.exceptions;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.containers.EmptyCheckContainer;

/**
 * Wraps return values from methods that might encounter exceptions.
 * This is usefull when methods should return a value or specific error code
 * for example.
 * <br>NOTE: Use of this class is not encouraged. It may be used under very 
 * specific circumstances; if in doubt, use {@link SafeReturn}.
 * 
 * @author Elwin Slokker
 *
 * @param <ExceptionT> Type of object that provides information about exceptions.
 * When this is a (extender of) {@link ExceptionInformation} use 
 * {@link SafeReturn} instead.
 * @param <ReturnT> The required return type. The type that returns when no 
 * exceptions occurred.
 */
public interface Exceptional<ExceptionT, ReturnT>
{

    /**
     * @return the value that was put into this object by the called method.
     */
    public abstract NullableWrapper<ReturnT> getReturn();

    /**
     * @return the exception that was put into this object by the called method.
     * <br>If there are no exceptions, simply returns an empty list.
     */
    public abstract EmptyCheckContainer<ExceptionT> getExceptions();
}
