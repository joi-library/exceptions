/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.exceptions;

import com.github.elwinbran.javaoverhaul.containers.Container;

/**
 * An object that makes/creates {@link Exceptional} objects.
 * <br><br>
 * There is really not much else to it. Simply pass what information the called
 * <br>method has to this object and it will make the right 
 * {@link Exceptional}.
 *
 * @author Elwin Slokker
 */
public interface ExceptionalFactory
{   
    /**
     * @param <ExceptionT> The type of exception object.
     * @param <ReturnT> The type of return when no exceptions are encountered.
     * @param value the value to pass to the caller.
     * @return a {@see Exceptional} that had no exceptions.
     */
    public <ExceptionT, ReturnT> Exceptional<ExceptionT, ReturnT> 
        make(ReturnT value);

    /**
     * @param <ExceptionT> The type of exception object.
     * @param <ReturnT> The type of return when no exceptions are encountered.
     * @param exceptionInformation the collection of information about the
     *                             exception(s).
     * @return a {@see Exceptional} that encountered a fatal exception.
     */
    public <ExceptionT, ReturnT> Exceptional<ExceptionT, ReturnT> 
        make(final Container<ExceptionT> exceptionInformation);

    /**
     * 
     * @param <ExceptionT> The type of exception object.
     * @param <ReturnT> The type of return when no exceptions are encountered.
     * @param exceptionInformation the array of information about the
     *                             exception(s).
     * @return a {@see Exceptional} that encountered a fatal exception.
     */
    public <ExceptionT, ReturnT> Exceptional<ExceptionT, ReturnT> 
        make(ExceptionT... exceptionInformation);

    /**
     *
     * @param <ExceptionT> The type of exception object.
     * @param <ReturnT> The type of return when no exceptions are encountered.
     * @param value the value to pass to the caller.
     * @param exceptionInformation the collection of information about the
     *                             exception(s).
     * @return a {@see Exceptional} that encountered a non-fatal
     * exception.
     */
    public <ExceptionT, ReturnT> Exceptional<ExceptionT, ReturnT>
        make(ReturnT value, 
                  Container<ExceptionT> exceptionInformation);

    /**
     * 
     * @param <ExceptionT> The type of exception object.
     * @param <ReturnT> The type of return when no exceptions are encountered.
     * @param value the value to pass to the caller.
     * @param exceptionInformation the array of information about the
     *                             exception(s).
     * @return a {@see Exceptional} that encountered a non-fatal
     * exception.
     */
    public <ExceptionT, ReturnT> Exceptional<ExceptionT, ReturnT> 
        make(ReturnT value, ExceptionT... exceptionInformation);
}
